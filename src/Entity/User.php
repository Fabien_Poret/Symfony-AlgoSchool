<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use phpDocumentor\Reflection\Types\Integer;

 /**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields="email", message="Cet email est déjà enregistré en base.")
 * @UniqueEntity(fields="username", message="Cet identifiant est déjà enregistré en base")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * 
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     */
     
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min="5", minMessage="Votre nom d'utilisateur dois contenir plus de 5 caractères")
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min="8", minMessage="Votre mot de passe dois contenir plus de 8 caractères")
     */
    private $password;

    /**
     *  @Assert\EqualTo(propertyPath="password", message="Votre mot de passe doit être identique à celui ci-dessus")
     */
    public $confirm_password;

    /**
     * @ORM\Column(type="integer")
     */
    public $role;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CommentForum", mappedBy="user")
     */
    private $getComment;

    public function __construct()
    {
        $this->getComment = new ArrayCollection();
    }


    public function getId()
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getRole(): ?Integer
    {
        return $this->role;
    }

    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function eraseCredentials()
    {
        
    }

    public function getSalt()
    {
        
    }

    public function getRoles()
    {
        return ['ROLE_USER'];
    }

    /**
     * @return Collection|CommentForum[]
     */
    public function getGetComment(): Collection
    {
        return $this->getComment;
    }

    public function addGetComment(CommentForum $getComment): self
    {
        if (!$this->getComment->contains($getComment)) {
            $this->getComment[] = $getComment;
            $getComment->setUser($this);
        }

        return $this;
    }

    public function removeGetComment(CommentForum $getComment): self
    {
        if ($this->getComment->contains($getComment)) {
            $this->getComment->removeElement($getComment);
            // set the owning side to null (unless already changed)
            if ($getComment->getUser() === $this) {
                $getComment->setUser(null);
            }
        }

        return $this;
    }
}
