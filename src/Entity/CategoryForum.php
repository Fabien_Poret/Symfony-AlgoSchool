<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryForumRepository")
 */
class CategoryForum
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Forum", mappedBy="categoryForum")
     */
    private $forums;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CommentForum", mappedBy="category")
     */
    private $date;

    public function __construct()
    {
        $this->forums = new ArrayCollection();
        $this->date = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Forum[]
     */
    public function getForums(): Collection
    {
        return $this->forums;
    }

    public function addForum(Forum $forum): self
    {
        if (!$this->forums->contains($forum)) {
            $this->forums[] = $forum;
            $forum->setCategoryForum($this);
        }

        return $this;
    }

    public function removeForum(Forum $forum): self
    {
        if ($this->forums->contains($forum)) {
            $this->forums->removeElement($forum);
            // set the owning side to null (unless already changed)
            if ($forum->getCategoryForum() === $this) {
                $forum->setCategoryForum(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CommentForum[]
     */
    public function getDate(): Collection
    {
        return $this->date;
    }

    public function addDate(CommentForum $date): self
    {
        if (!$this->date->contains($date)) {
            $this->date[] = $date;
            $date->setCategory($this);
        }

        return $this;
    }

    public function removeDate(CommentForum $date): self
    {
        if ($this->date->contains($date)) {
            $this->date->removeElement($date);
            // set the owning side to null (unless already changed)
            if ($date->getCategory() === $this) {
                $date->setCategory(null);
            }
        }

        return $this;
    }
}
