<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ForumRepository")
 * @ORM\Table(name="forum")
 */
class Forum
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $author;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CategoryForum", inversedBy="forums")
     * @ORM\JoinColumn(nullable=false)
     */
    private $categoryForum;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CommentForum", mappedBy="forum")
     */
    private $commentForums;

    public function __construct()
    {
        $this->commentForums = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function setAuthor(string $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCategoryForum(): ?CategoryForum
    {
        return $this->categoryForum;
    }

    public function setCategoryForum(?CategoryForum $categoryForum): self
    {
        $this->categoryForum = $categoryForum;

        return $this;
    }

    /**
     * @return Collection|CommentForum[]
     */
    public function getCommentForums(): Collection
    {
        return $this->commentForums;
    }

    public function addCommentForum(CommentForum $commentForum): self
    {
        if (!$this->commentForums->contains($commentForum)) {
            $this->commentForums[] = $commentForum;
            $commentForum->setForum($this);
        }

        return $this;
    }

    public function removeCommentForum(CommentForum $commentForum): self
    {
        if ($this->commentForums->contains($commentForum)) {
            $this->commentForums->removeElement($commentForum);
            // set the owning side to null (unless already changed)
            if ($commentForum->getForum() === $this) {
                $commentForum->setForum(null);
            }
        }

        return $this;
    }
}
