<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Level;
use App\Entity\Article;
use App\Entity\CommentBlog;
use App\Repository\CommentBlogRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Security;
use App\Repository\ArticleRepository;
use App\Repository\LevelRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends Controller
{
     /**
     * @Route("/admin", name="admin")
     */
    public function index(Security $security, UserRepository $repoUser, LevelRepository $repoLevel)
    {
        if($security->getUser()){ 
            $user = $security->getUser()->getUsername();
            $roleUser = $repoUser->findOneBy(['username' => $user]);
            $roleUser = $roleUser->role;
        }
        else {
            $roleUser = "Non connecté";
            $user = "Non connecté";
        }

        $usersCount = count($repoUser->findAll());
        $levelCount = count($repoLevel->findAll());

        $usersConnect = $usersCount - 2;


        return $this->render('admin/dashboard.html.twig', [
            'controller_name' => 'AdminController',
            'roleUser' => $roleUser,
            'levelCount' => $levelCount,
            'user' => $user,
            'usersConnect' => $usersConnect,
            'usersCount' => $usersCount
        ]);
    }


    /**
     * @Route("/blogAdmin", name="blogAdmin")
     */
    public function blogAdmin(Security $security, UserRepository $repoUser, ArticleRepository $repo, Request $request, ObjectManager $manager)
    {
        if($security->getUser()){ 
            $user = $security->getUser()->getUsername();
            $roleUser = $repoUser->findOneBy(['username' => $user]);
            $roleUser = $roleUser->role;
        }
        else {
            $roleUser = "Non connecté";
            $user = "Non connecté";
        }

        $articles = $repo->findAll();

        if($request->isMethod('POST')) {
            if($request->request->has('blog_id')) {
                $idArticle = $request->request->get('blog_id');
                $actionArticle = ($request->request->has("deleteArticle")) ? 'deleteArticle' : 'editArticle';
                if($actionArticle === 'deleteArticle') {
                    $article = $repo->findById($idArticle);
                    $manager->remove($article[0]);
                    $manager->flush();
                    return $this->redirectToRoute('blogAdmin');
                }
            }
        }

        return $this->render('admin/blogAdmin.html.twig', [
            'controller_name' => 'AdminController',
            'roleUser' => $roleUser,
            'articles' => $articles,
            'user' => $user
        ]);
    }

    /**
     * @Route("/character", name="character")
     */
    public function character(Security $security, UserRepository $repoUser)
    {
        if($security->getUser()){ 
            $user = $security->getUser()->getUsername();
            $roleUser = $repoUser->findOneBy(['username' => $user]);
            $roleUser = $roleUser->role;
        }
        else {
            $roleUser = "Non connecté";
            $user = "Non connecté";
        }

        return $this->render('admin/game/character.html.twig', [
            'controller_name' => 'AdminController',
            'roleUser' => $roleUser,
            'user' => $user
        ]);
    }


}
