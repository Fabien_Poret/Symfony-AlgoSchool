<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Repository\CommentBlogRepository;
use App\Repository\CommentForumRepository;
use App\Entity\User;
use App\Repository\ForumRepository;
class UserController extends AbstractController
{
    /**
     * @Route("/user", name="user")
     */
    public function index()
    {
        return $this->render('admin/user.html.twig', [
            'controller_name' => 'UserController',
        ]);
    }

    /**
     * @Route("/adminMyUsers", name="adminMyUsers")
     */
    public function adminMyUsers(Security $security, UserRepository $repoUser, Request $request, ObjectManager $manager, CommentBlogRepository $repoBlog, ForumRepository $repoForum)
    {
        if($security->getUser()){ 
            $myLogs = $security->getUser()->getUsername();
            $roleUser = $repoUser->findOneBy(['username' => $myLogs]);
            $roleUser = $roleUser->role;
        }
        else {
            $roleUser = "Non connecté";
            $user = "Non connecté";
        }
        $allUsers = $repoUser->findAll();


        if($request->isMethod('POST')) {
            $user_id = $request->request->get('user_id');

            $userRemove = $repoUser->findById($user_id);
            

            // $userRemove[0]->eraseCredentials();
            // $userCommentBlogs = [];
            // $boucles = $repoBlog->findAll();
            // dump($boucles);
            // foreach($userCommentBlogs as $boucles)
            // {
 
            // }
            // dump($userCommentBlog);
            $manager->remove($userRemove[0]);
            $manager->flush();

            return $this->redirectToRoute('adminMyUsers');

        }




        return $this->render('admin/users/adminMyUsers.html.twig', [
            'controller_name' => 'AdminController',
            'roleUser' => $roleUser,
            'allUsers' => $allUsers,
            'user' => $myLogs
        ]);
    }


    /**
     * @Route("/skillOfMyUsers", name="skillOfMyUsers")
     */
    public function skillOfMyUsers(Security $security, UserRepository $repoUser)
    {
        if($security->getUser()){ 
            $user = $security->getUser()->getUsername();
            $roleUser = $repoUser->findOneBy(['username' => $user]);
            $roleUser = $roleUser->role;
        }
        else {
            $roleUser = "Non connecté";
            $user = "Non connecté";
        }

        return $this->render('admin/users/skillOfMyUsers.html.twig', [
            'controller_name' => 'AdminController',
            'roleUser' => $roleUser,
            'user' => $user
        ]);
    }


    /**
     * @Route("/myProfil", name="myProfil")
     */
    public function myProfil(Security $security, UserRepository $repoUser, Request $request, ObjectManager $manager)
    {
        if($security->getUser()){ 
            $myLogs = $security->getUser()->getUsername();
            $roleUser = $repoUser->findOneBy(['username' => $myLogs]);
            $roleUser = $roleUser->role;
        }
        else {
            $roleUser = "Non connecté";
            $myLogs = "Non connecté";
        }

        $userConnect = $security->getUser();

        return $this->render('admin/users/myProfil.html.twig', [
            'controller_name' => 'AdminController',
            'roleUser' => $roleUser,
            'userConnect' => $userConnect,
            'user' => $myLogs
        ]);
    }

    /**
     * @Route("/myParam", name="myParam")
     */
    public function myParam(Security $security, UserRepository $repoUser)
    {
        if($security->getUser()){ 
            $user = $security->getUser()->getUsername();
            $roleUser = $repoUser->findOneBy(['username' => $user]);
            $roleUser = $roleUser->role;
        }
        else {
            $roleUser = "Non connecté";
            $user = "Non connecté";
        }

        return $this->render('admin/users/myParam.html.twig', [
            'controller_name' => 'AdminController',
            'roleUser' => $roleUser,
            'user' => $user
        ]);
    }


    /**
     * @Route("/upgrade", name="upgrade")
     */
    public function upgrade(Security $security, UserRepository $repoUser, Request $request, ObjectManager $manager)
    {
       
        if($security->getUser()){ 
            $user = $security->getUser()->getUsername();
            $roleUser = $repoUser->findOneBy(['username' => $user]);
            $roleUser = $roleUser->role;
        }
        else {
            $roleUser = "Non connecté";
            $user = "Non connecté";
        }



        if($request->isMethod('POST')) {
            $radio = $request->request->get('accountType');
            $userConnect = $security->getUser();
            $userConnect->setRole($radio);

            // $user->setRole($accountType);
            // $user->setUsername($_username);
            // $user->setEmail($_email);
            // $user->setPassword($hash);

            $manager->persist($userConnect);
            $manager->flush();

            return $this->redirectToRoute('admin');

        }

        return $this->render('admin/users/upgrade.html.twig', [
            'controller_name' => 'AdminController',
            'roleUser' => $roleUser,
            'user' => $user
        ]);
    }

    /**
     * @Route("/user_create", name="user_create")
     */
    public function user_create(Security $security, UserRepository $repoUser, Request $request, ObjectManager $manager)
    {
        if($security->getUser()){ 
            $myLog = $security->getUser()->getUsername();
            $roleUser = $repoUser->findOneBy(['username' => $myLog]);
            $roleUser = $roleUser->role;
        }
        else {
            $roleUser = "Non connecté";
            $myLog = "Non connecté";
        }

        if($request->isMethod('POST')) {
            $className = $request->request->get('className');
            $pwd = $request->request->get('pwd');
            $mailTmp = $request->request->get('mail');
            // $mail = sprint('%s@algoschool.fr', $mailTmp);
            $mail = $mailTmp . '@algoschool.fr';
            $role = $request->request->get('role');
            $count = $request->request->get('count');
            $comment = $request->request->get('comment');

            for ($i = 0; $i < $count; $i++) {
                $user = new User();
                $user->setEmail($i . $mail);
                $user->setUsername($className);
                $user->setRole($role);
                $user->setPassword($pwd);
                $manager->persist($user);
                $manager->flush();

            }

            $userConnect = $security->getUser();
            return $this->redirectToRoute('adminMyUsers');

        }

        return $this->render('admin/users/user_create.html.twig', [
            'controller_name' => 'AdminController',
            'roleUser' => $roleUser,
            'user' => $myLog
        ]);
    }
}
