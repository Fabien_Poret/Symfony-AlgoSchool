<?php

namespace App\Controller;

use App\Form\NewsletterType;
use App\Repository\NewsletterRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Newsletter;

class HomeController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function home(Request $request, ObjectManager $manager) {

        $newsletter = new Newsletter();
        if($request->isMethod('POST')) {
            $email = $request->request->get('nw-email');
            $newsletter->setMail($email);
            $manager->persist($newsletter);
            $manager->flush();
            // return $this->redirectToRoute('home');
        }
        // $form = $this->createForm(NewsletterType::class, $newsletter);
        // $form->handleRequest($request);

        // if($form->isSubmitted() && $form->isValid()) {
        //     var_dump($newsletter);
        //     exit;
        //     $manager->persist($newsletter);
        //     $manager->flush();
        //     return $this->redirectToRoute('home');
        // }

        return $this->render('pages/home.html.twig', [
            'title' => "Bienvenu ici les amis !"
        ]);
    }

    /**
     * @Route("/team", name="team")
     */
    public function team(Request $request, ObjectManager $manager) {

        return $this->render('pages/team.html.twig', [
            'title' => "Bienvenu ici les amis !"
        ]);
    }

    
    /**
     * @Route("/mention", name="mention")
     */
    public function mention(Request $request, ObjectManager $manager) {

        return $this->render('pages/mention.html.twig', [
            'title' => "Bienvenu ici les amis !"
        ]);
    }

    
    /**
     * @Route("/discover", name="discover")
     */
    public function discover(Request $request, ObjectManager $manager) {

        return $this->render('pages/discover.html.twig', [
            'title' => "Bienvenu ici les amis !"
        ]);
    }
    /**
     * @Route("/faq", name="faq")
     */
    public function faq(Request $request, ObjectManager $manager) {

        return $this->render('pages/faq.html.twig', [
            'title' => "Bienvenu ici les amis !"
        ]);
    }

    /**
     * @Route("/error", name="error")
     */
    public function error(Request $request, ObjectManager $manager) {

        return $this->render('pages/error.html.twig', [
            'title' => "Bienvenu ici les amis !"
        ]);
    }

    /**
     * @Route("/choice", name="choice")
     */
    public function choice(Request $request, ObjectManager $manager) {

        return $this->render('pages/choice.html.twig', [
            'title' => "Bienvenu ici les amis !"
        ]);
    }

    
    /**
     * @Route("/price", name="price")
     */
    public function price(Request $request, ObjectManager $manager) {

        return $this->render('pages/price.html.twig', [
            'title' => "Bienvenu ici les amis !"
        ]);
    }
}
