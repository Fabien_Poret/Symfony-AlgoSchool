<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Security;

class GameController extends AbstractController
{
    /**
     * @Route("/game", name="game")
     */
    public function index()
    {
        $heightLine = '50px';
        $widthLine =  '100px';
        $x1 = '50px';
        $x2 = '50px';
        $y1 = '0px';
        $y2 = '100px';
        $styleLineFalse = 'stroke:rgb(255, 78, 62);stroke-width:2';
        $styleLineTrue = 'stroke:rgb(78, 192, 91);stroke-width:2';

        $msg = "Algorithmique AlgoSchool";

        return $this->render('game/game.html.twig', [
            'controller_name' => 'GameController',
            'heightLine' => $heightLine,
            'widthLine' => $widthLine,
            'x1' => $x1,
            'x2' => $x2,
            'y1' => $y1,
            'y2' => $y2,
            'styleLineFalse' => $styleLineFalse,
            'styleLineTrue' => $styleLineTrue,
             'msg' => $msg,
        ]);
    }

    /**
     * @Route("/createLvl", name="createLvl")
     */
    public function createLvl(Security $security, UserRepository $repoUser)
    {
        if($security->getUser()){ 
            $user = $security->getUser()->getUsername();
            $roleUser = $repoUser->findOneBy(['username' => $user]);
            $roleUser = $roleUser->role;
        }
        else {
            $roleUser = "Non connecté";
            $user = "Non connecté";
        }

        return $this->render('admin/game/createLvl.html.twig', [
            'controller_name' => 'GameController',
            'roleUser' => $roleUser,
            'user' => $user
        ]);
    }

    /**
     * @Route("/editLvl", name="editLvl")
     */
    public function editLvl(Security $security, UserRepository $repoUser)
    {
        if($security->getUser()){ 
            $user = $security->getUser()->getUsername();
            $roleUser = $repoUser->findOneBy(['username' => $user]);
            $roleUser = $roleUser->role;
        }
        else {
            $roleUser = "Non connecté";
            $user = "Non connecté";
        }

        return $this->render('admin/game/editLvl.html.twig', [
            'controller_name' => 'GameController',
            'roleUser' => $roleUser,
            'user' => $user
        ]);
    }

    /**
     * @Route("/showLvl", name="showLvl")
     */
    public function showLvl(Security $security, UserRepository $repoUser)
    {
        if($security->getUser()){ 
            $user = $security->getUser()->getUsername();
            $roleUser = $repoUser->findOneBy(['username' => $user]);
            $roleUser = $roleUser->role;
        }
        else {
            $roleUser = "Non connecté";
            $user = "Non connecté";
        }

        return $this->render('admin/game/showLvl.html.twig', [
            'controller_name' => 'GameController',
            'roleUser' => $roleUser,
            'user' => $user
        ]);
    }

    /**
     * @Route("/maquette", name="maquette")
     */
    public function maquette()
    {
        return $this->render('pages/maquette.html.twig', [
            'controller_name' => 'GameController',
        ]);
    }
}
