<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Form\RegistrationType;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\Article;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends Controller
{
    /**
     * @Route("/inscription", name="security_registration")
     */
    public function registration(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $encoder){
        $user = new User();
        $etatMode = 0;
        if($request->isMethod('POST')) {
            
            $accountType = 0;
            $_username = $request->request->get('_username');
            $_email = $request->request->get('_email');
            $password = $request->request->get('password');
            $samePwd = $request->request->get('samePwd');
            if($password !== $samePwd)
            {
                $etatMode = 1;
                return $this->redirectToRoute('security_registration');  
            }
            $hash = $encoder->encodePassword($user, $password);

            $user->setRole($accountType);
            $user->setUsername($_username);
            $user->setEmail($_email);
            $user->setPassword($hash);

            $manager->persist($user);
            $manager->flush();

            return $this->redirectToRoute('security_login');

        }

        // $form = $this->createForm(RegistrationType::class, $user);
        // $form->handleRequest($request);
        // if($form->isSubmitted() && $form->isValid()) {
        //     $hash = $encoder->encodePassword($user, $user->getPassword());
        //     $user->setPassword($hash);
        //     $manager->persist($user);
        //     $manager->flush();

        //     return $this->redirectToRoute('security_login');
        // }



        return $this->render('security/registration.html.twig', [
            // 'formInscription' => $form->createView()
            'etatMode' => $etatMode
        ]);
    }

    /**
     * @Route("/connexion", name="security_login")
     */
    public function login(AuthenticationUtils $authenticationUtils)
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
        ));
    }
    /**
     * @Route("/deconnexion", name="security_logout")
     */
    public function logout() {}
}
