<?php

namespace App\Controller;

use App\Entity\Forum;
use App\Entity\CategoryForum;
use App\Repository\ForumRepository;
use App\Repository\CategoryForumRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\CommentForumRepository;
use App\Entity\CommentForum;
use App\Form\CommentForumType;
use App\Form\ForumPostType;
use Symfony\Component\Security\Core\Security;
use App\Repository\UserRepository;

class ForumController extends Controller
{
    /**
     * @Route("/forum", name="forum")
     */
    public function index(CategoryForumRepository $repoCat, ForumRepository $repoForum)
    {
        $getCats = $repoCat->findAll();
        $getForums = $repoForum->findAll();

        $forums = [];
        $categoryOne = [];
        $categoryTwo = [];
        $categoryThree = [];
        foreach( $getForums as $getForum)
        {
            // Get forum post in "Apprentie dév"
           if($getForum->getCategoryForum()->getId() === 1)
           {
            $dateTime = $getForum->getDate();
            $dateString = date_format($dateTime, 'Y-m-d H:i:s');

                $forums[] = $categoryOne [] = [
                    'id' => $getForum->getId(),
                    'title' => $getForum->getTitle(),
                    'author' => $getForum->getAuthor(),
                    'date' => $dateString,
                    'content' => $getForum->getContent(),
                    'titleCategory' => $getForum->getCategoryForum()->getTitle(),
                    'idCategory' => $getForum->getCategoryForum()->getId(),
               ];
           }

           if($getForum->getCategoryForum()->getId() === 2)
           {
                $dateTime = $getForum->getDate();   
                $dateString = date_format($dateTime, 'Y-m-d H:i:s');

                $forums[] = $categoryTwo [] = [
                    'id' => $getForum->getId(),
                    'title' => $getForum->getTitle(),
                    'author' => $getForum->getAuthor(),
                    'date' => $dateString,
                    'content' => $getForum->getContent(),
                    'titleCategory' => $getForum->getCategoryForum()->getTitle(),
                    'idCategory' => $getForum->getCategoryForum()->getId(),
               ];
           }

           if($getForum->getCategoryForum()->getId() === 3)
           {
               $dateTime = $getForum->getDate();
               $dateString = date_format($dateTime, 'Y-m-d H:i:s');

                $forums[] = $categoryThree [] = [
                    'id' => $getForum->getId(),
                    'title' => $getForum->getTitle(),
                    'author' => $getForum->getAuthor(),
                    'date' => $dateString,
                    'content' => $getForum->getContent(),
                    'titleCategory' => $getForum->getCategoryForum()->getTitle(),
                    'idCategory' => $getForum->getCategoryForum()->getId(),
               ];
           }    
        }

        return $this->render('pages/forum/forum.html.twig', [
            'categoryOne' => $categoryOne,
            'categoryTwo' => $categoryTwo,
            'categoryThree' => $categoryThree,
            'forums' => $forums
        ]);
    }

    /**
     * @Route("/forum/categoryGame", name="forum_one")
     */
    public function categoryOne(ForumRepository $repo)
    {
        $getForums = $repo->findBy(
            ['categoryForum' => '1']
        );

        $forums = [];
        foreach($getForums as $getForum)
        {
            $dateTime = $getForum->getDate();
            $dateString = date_format($dateTime, 'Y-m-d H:i:s');

            $forums[] = [
                'id' => $getForum->getId(),
                'title' => $getForum->getTitle(),
                'author' => $getForum->getAuthor(),
                'date' => $dateString,
                'content' => $getForum->getContent(),
                'titleCategory' => $getForum->getCategoryForum()->getTitle(),
                'idCategory' => $getForum->getCategoryForum()->getId(),
            ];          
        }

        return $this->render('pages/forum/show/showGame.html.twig', [
            'controller_name' => 'one',
            'forums' => $forums
        ]);
    }

    /**
     * @Route("/forum/categorySoluce", name="forum_two")
     */
    public function categoryTwo(ForumRepository $repo)
    {
        $getForums = $repo->findBy(
            ['categoryForum' => '2']
        );

        $forums = [];
        foreach($getForums as $getForum)
        {
            $dateTime = $getForum->getDate();
            $dateString = date_format($dateTime, 'Y-m-d H:i:s');

            $forums[] = [
                'id' => $getForum->getId(),
                'title' => $getForum->getTitle(),
                'author' => $getForum->getAuthor(),
                'date' => $dateString,
                'content' => $getForum->getContent(),
                'titleCategory' => $getForum->getCategoryForum()->getTitle(),
                'idCategory' => $getForum->getCategoryForum()->getId(),
            ];          
        }

        return $this->render('pages/forum/show/showSoluce.html.twig', [
            'controller_name' => 'two',
            'forums' => $forums
        ]);
    }
    /**
     * @Route("/forum/categoryTeachers", name="forum_three")
     */
    public function categoryThree(ForumRepository $repo)
    {
        $getForums = $repo->findBy(
            ['categoryForum' => '3']
        );

        $forums = [];
        foreach($getForums as $getForum)
        {
            $dateTime = $getForum->getDate();
            $dateString = date_format($dateTime, 'Y-m-d H:i:s');

            $forums[] = [
                'id' => $getForum->getId(),
                'title' => $getForum->getTitle(),
                'author' => $getForum->getAuthor(),
                'date' => $dateString,
                'content' => $getForum->getContent(),
                'titleCategory' => $getForum->getCategoryForum()->getTitle(),
                'idCategory' => $getForum->getCategoryForum()->getId(),
            ];          
        }



        return $this->render('pages/forum/show/showTeachers.html.twig', [
            'controller_name' => 'three',
            'forums' => $forums
        ]);
    }

    /**
     * @Route("/forum/new", name="forum_create_post")
     */
    public function formPost()
    {
        return $this->render('pages/forum/create.html.twig', [
            'controller_name' => 'ForumController'
        ]);
    }

    /**
     * @Route("/forum/{id}", name="forum_show")
     */
    public function forumShow(CategoryForumRepository $repoCat, UserRepository $repoUser, ForumRepository $repo, Request $request, Forum $getForum, CommentForumRepository $commentForumRepo, CommentForum $commentForum = null,  ObjectManager $manager, Security $security)
    {
        if(!$commentForum){
            $commentForum = new commentForum(); 
        }

        $comments = $commentForumRepo->findBy(   
            ['forum' => $getForum->getId()]
        );

        if($request->isMethod('POST')) {
            if($request->request->has('forum_id')) {
                $idForum = $request->request->get('forum_id');
                $actionForum = ($request->request->has("deleteForum")) ? 'deleteForum' : 'editForum';
                if($actionForum === 'deleteForum') {
                    $forum = $repo->findById($idForum);

                    foreach($comments as $comment) {
                        $manager->remove($comment);
                    }

                    $manager->remove($forum[0]);
                    $manager->flush();
                    return $this->redirectToRoute('forum');
                }
            }

            if($request->request->has('com_id')) {
                $id = $request->request->get('com_id');
                $action = ($request->request->has("delete")) ? 'delete' : 'edit';

                if($action === 'delete') {
                    $commentForum = $commentForumRepo->findById($id);
                    $manager->remove($commentForum[0]);
                    $manager->flush();
                    return $this->redirectToRoute('forum_show', ['id' => $getForum->getId()]);
                }
            }
        }

        //Créer le form
        $form = $this->createForm(CommentForumType::class, $commentForum);

        //Créer la vue sur twig
        //Recevoir avec un request les données du commentaire
        //l'enregistrer dans la bdd
        
        $dateTime = $getForum->getDate();
        $dateString = date_format($dateTime, 'Y-m-d H:i:s');

        $categoryTitle = $getForum->getCategoryForum()->getTitle();
        $category = $repoCat->findOneBy(['title' => $categoryTitle]);

        $forum = [
            'id' => $getForum->getId(),
            'title' => $getForum->getTitle(),
            'author' => $getForum->getAuthor(),
            'date' => $dateString,
            'content' => $getForum->getContent(),
            'titleCategory' => $getForum->getCategoryForum()->getTitle(),
            'idCategory' => $getForum->getCategoryForum()->getId(),
        ];     

        if($security->getUser()){ 
            $user = $security->getUser()->getUsername();
            $roleUser = $repoUser->findOneBy(['username' => $user]);
            // $roleUser = $roleUser->role;
        }
        else {
            $roleUser = "Non connecté";
            $user = "Non connecté";
        }

        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()) 
        {
            $commentForum->SetDate(new \DateTime());
            $commentForum->setForum($getForum);
            $commentForum->setUser($roleUser);
            $commentForum->setCategory($category);
            $manager->persist($commentForum);
            $manager->flush();

            return $this->redirectToRoute('forum_show', ['id' => $getForum->getId()]);
        }



        // dump($forum);
        // exit;

        return $this->render('pages/forum/show/showForum.html.twig', [
            'formComment' => $form->createView(),
            'roleUser' => $roleUser,
            'comments'=> $comments,
            'commentUser' => $commentForum,
            'forum' => $forum,
            'user' => $user
        ]);
    }



    /**
     * @Route("/create", name="forum_create")
     */
    public function createForum(ObjectManager $manager, CategoryForumRepository $repoCat, Forum $forum  = null, Request $request, Security $security, UserRepository $repoUser){
    
        // Créer un formulaire 
        if(!$forum){
            $forum = new Forum(); 
        }

        $form = $this->createForm(ForumPostType::class, $forum);
        $form->handleRequest($request);

        if($security->getUser()){ 
            $user = $security->getUser()->getUsername();
            $roleUser = $repoUser->findOneBy(['username' => $user]);
            // $roleUser = $roleUser->role;
        }
        else {
            $roleUser = "Non connecté";
            $user = "Non connecté";
        }


        if($form->isSubmitted() && $form->isValid()) {

            $idCat = $request->request->get('category');
            $category = $repoCat->findBy(   
                ['id' => $idCat]
            );

            $forum->setDate(new \DateTime());
            $forum->setCategoryForum($category[0]);
            $forum->setAuthor($user);
            dump($forum);
            $manager->persist($forum);
            $manager->flush();
            return $this->redirectToRoute('forum_show', ['id' => $forum->getId()]);
        }
        return $this->render('pages/forum/create.html.twig', [
            'formCreate' => $form->createView(),
        ]);
    }
}
