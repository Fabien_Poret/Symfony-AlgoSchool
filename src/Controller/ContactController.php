<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ContactController extends Controller
{
    /**
     * @Route("/contact", name="contact")
     */
    public function index( Request $request, \Swift_Mailer $mailer)
    {


        if($request->isMethod('POST')) {
            $name = $request->request->get('name');
            $email = $request->request->get('email');
            $object = $request->request->get('object');
            $subject = $request->request->get('subject');


            // $message = \Swift_Message::newInstance()
            // ->setContentType('text/html')
            // ->setSubject($subject)
            // ->setFrom($email)
            // ->setTo('AlgoSchool@gmail.com')
            // ->setBody($name . $subject);

            // $this->get('mailer')->send($message);
        }


        return $this->render('pages/contact.html.twig', [
            'controller_name' => 'ContactController',
        ]);
    }
}
