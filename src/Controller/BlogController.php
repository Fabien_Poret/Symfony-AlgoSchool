<?php

namespace App\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Article;
use App\Repository\ArticleRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Form\ArticleType;
use App\Entity\CommentBlog;
use App\Form\CommentBlogType;
use App\Form\DeleteCommentType;
use Symfony\Component\Security\Core\Security;
use App\Repository\CommentBlogRepository;
use App\Entity\Category;
use App\Repository\UserRepository;

class BlogController extends Controller
{
    /**
     * @Route("/blog", name="blog")
     */
    public function index(ArticleRepository $repo)
    {
        $articles = $repo->findAll();
        $reverse = array_reverse($articles);
        
        // dump($reverse);
        // exit;

        return $this->render('pages/blog/blog.html.twig', [
            'controller_name' => 'BlogController',
            'articles' => $reverse
        ]);
    }


    /**
     * @Route("/blog/new", name="blog_create")
     * @Route("/blog/{id}/edit", name="blog_edit")
     */
    public function formArticle(CommentBlogRepository $repo ,Article $article = null, Request $request, ObjectManager $manager, CommentBlog $commentblog = null, Security $security, UserRepository $repoUser) {
        
        if(!$article){
            $article = new Article(); 
        }
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);
        
        if($request->isMethod('POST')) {
            if($request->request->has('com_id')) {
                $id = $request->request->get('com_id');
                $action = ($request->request->has("delete")) ? 'delete' : 'edit';
        
                if($action === 'delete') {
                    $commentblog = $repo->findById($id);
                    $manager->remove($commentblog[0]);
                    $manager->flush();
                    return $this->redirectToRoute('blog_edit', ['id' => $article->getId()]);
                }
            }
        }

        if($form->isSubmitted() && $form->isValid()) {
            $article->setCreatedAt(new \DateTime());
            dump($article);
            $manager->persist($article);
            $manager->flush();
            return $this->redirectToRoute('blog_show', ['id' => $article->getId()]);
        }

        if($security->getUser()){ 
            $user = $security->getUser()->getUsername();
            $roleUser = $repoUser->findOneBy(['username' => $user]);
            $roleUser = $roleUser->role;
        } else {
            $roleUser = "Non connecté";
            $user = "Non connecté";
        }

        return $this->render('pages/blog/create.html.twig', [
            'formArticle' => $form->createView(),
            'article' => $article,
            'editMode' => $article->getId() !== null,
            'roleUser' => $roleUser,
            'user' => $user
        ]);
    }




    /**
     * @Route("/blog/{id}", name="blog_show")
     */
    public function show(UserRepository $repoUser, ArticleRepository $repo, CommentBlogRepository $repoComment, Article $article, CommentBlog $commentblog = null, Request $request, ObjectManager $manager, Security $security) {

        if(!$commentblog){
            $commentblog = new commentblog(); 
        }

        if($request->isMethod('POST')) {
            if($request->request->has('blog_id')) {
                $idArticle = $request->request->get('blog_id');
                $actionArticle = ($request->request->has("deleteArticle")) ? 'deleteArticle' : 'editArticle';
                if($actionArticle === 'deleteArticle') {
                    $article = $repo->findById($idArticle);
                    $manager->remove($article[0]);
                    $manager->flush();
                    return $this->redirectToRoute('blog');
                }
            }

            if($request->request->has('com_id')) {
                $id = $request->request->get('com_id');
                $action = ($request->request->has("delete")) ? 'delete' : 'edit';

                if($action === 'delete') {
                    $commentblog = $repoComment->findById($id);
                    $manager->remove($commentblog[0]);
                    $manager->flush();
                    return $this->redirectToRoute('blog_show', ['id' => $article->getId()]);
                }
            }
        }

        $form = $this->createForm(CommentBlogType::class, $commentblog);

        $form->handleRequest($request);

        if($security->getUser()){ 
            $user = $security->getUser()->getUsername();
            $roleUser = $repoUser->findOneBy(['username' => $user]);
            $roleUser = $roleUser->role;
        }
        else {
            $roleUser = "Non connecté";
            $user = "Non connecté";
        }

        if($form->isSubmitted() && $form->isValid()) {
            $commentblog->setCretedAt(new \DateTime());
            $commentblog->setArticle($article);
            $commentblog->setAuthor($user);
            $manager->persist($commentblog);
            $manager->flush();

            return $this->redirectToRoute('blog_show', ['id' => $article->getId()]);
        }
        // Gestion role utilisateur 

        return $this->render('pages/blog/show.html.twig', [
            'formBlog' => $form->createView(),
            'roleUser' => $roleUser,
            'user' => $user,
            'article' => $article
        ]);
    }

    
}
