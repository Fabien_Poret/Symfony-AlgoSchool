<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Article;
use App\Entity\Category;
use App\Entity\CommentBlog;

class ArticlesFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create('fr_FR');
        
        for($i = 1; $i <= 3; $i++) {
            $category = new Category();
            $category->setTitle($faker->sentence())
                     ->setDescription($faker->paragraph());
            $manager->persist($category);

            for($j = 1; $j <= mt_rand(4, 6); $j++) {
                $article = new Article();
                $content = join($faker->paragraphs(5));
                $article->setTitle($faker->sentence())
                        ->setContent($content)
                        ->setImage($faker->imageUrl)
                        ->setCreatedAt($faker->dateTimeBetween('-6 months'))
                        ->setCategory($category);
                $manager->persist($article);

                for($k=1; $k <= mt_rand(4, 10); $k++) {
                    $commentBlog = new CommentBlog();

                    $content .= join($faker->paragraphs(5));
                    $now = new \DateTime();
                    $interval = $now->diff($article->getCreatedAt());
                    $days = $interval->days;
                    $minimum = '-' . $days . 'days'; 

                    $commentBlog->setAuthor($faker->name)
                                ->setContent($content)
                                ->setCretedAt($faker->dateTimeBetween($minimum))
                                ->setArticle($article);

                    $manager->persist($commentBlog);
                }
            }
        }
        $manager->flush();
    }
}
