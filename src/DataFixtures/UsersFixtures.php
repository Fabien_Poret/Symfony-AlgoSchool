<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UsersFixtures extends Fixture
{
    const EMAIL = array(
        0 => "neka.algo@gmail.com",
        1 => "pierre.algo@gmail.com",
        2 => "arnaud.algo@gmail.com",
        3 => "quentin.algo@gmail.com",
        4 => "fabien.algo@gmail.com",
    );
    public function load(ObjectManager $manager)
    {
        // $manager->persist($product);
        $email = self::EMAIL;

        $password = array(
            0 => "NekaAlgoschool",
            1 => "PierreAlgoschool",
            2 => "ArnaudAlgoschool",
            3 => "QuentinAlgoschool",
            4 => "FabienAlgoschool",
        );

        for($i = 0; $i < 5; $i++){
            $user = new User();
            $user->setEmail($email[$i])
                ->setPassword('$2y$13$m3CA6IogvudesXAVfD8.Z.vbpyc7PWDxab5eneW.DfPNwGzh/aDqi')
                ->setRole(4)
                ->setUsername('AlgoSchool');
            $manager->persist($user);

            $this->addReference($user->getEMail(), $user);
        }

        $manager->flush();
    }
}
