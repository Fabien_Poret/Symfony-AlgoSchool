<?php

namespace App\DataFixtures;

use App\Entity\Forum;
use App\Entity\CategoryForum;
use App\Entity\CommentForum;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class ForumFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create('fr_FR');

        for($i = 1; $i <= 3; $i++) {
            $categoryForum = new CategoryForum();
            $categoryForum->setTitle($faker->sentence())
                        ->setDescription($faker->paragraph());
            $manager->persist($categoryForum);

            for($j = 1; $j <= mt_rand(4, 6); $j++) {
                $postForum = new Forum();
                $content = join($faker->paragraphs(2));
                $postForum->setTitle($faker->sentence())
                        ->setContent($content)
                        ->setAuthor($faker->name)
                        ->setDate($faker->dateTimeBetween('-6 months'))
                        ->setCategoryForum($categoryForum);
                $manager->persist($postForum);

                for($k=1; $k <= mt_rand(4, 10); $k++) {
                    $commentForum = new CommentForum();

                    $content .= join($faker->paragraphs(1));
                    $now = new \DateTime();
                    $interval = $now->diff($postForum->getDate());
                    $days = $interval->days;
                    $minimum = '-' . $days . 'days'; 

                    $user = $this->getReference(UsersFixtures::EMAIL[array_rand(UsersFixtures::EMAIL)]);
                    $commentForum->setUser($user)
                                ->setCategory($categoryForum)
                                ->setContent($content)
                                ->setDate($faker->dateTimeBetween($minimum))
                                ->setForum($postForum);

                    $manager->persist($commentForum);
            }
            }
        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            UsersFixtures::class,
        );
    }
}
