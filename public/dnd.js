
var widthPicture = "60px";
var n = 0;

var one = 0;
var two = 0;
var tree = 0;
var four = 0;

function dragStart(event) {
    event.dataTransfer.setData("Text", event.target.id);
    document.getElementById("demo").innerHTML = "Le DND commence";
}

function dragEnd(event) {
    document.getElementById("demo").innerHTML = "DND terminé";
}

function allowDrop(event) {
    event.preventDefault();
}

function drop(event, state, action) {
    var state = state;
    var action = action;
    event.preventDefault();
    var data = event.dataTransfer.getData("Text");
    event.target.appendChild(document.getElementById(data));
    var tmpIdPicture =  $("#state"+state+" img").attr("id");
    var idPicture = tmpIdPicture.split('-');
    var srcPicture = $("#state"+state+" img").attr("src");
    console.log(idPicture);

    switch(state)
    {
        case 1 :
            if(action == idPicture[0])
            {
                n++;
                $("#actContainer").prepend('<img ondragstart="dragStart(event)" ondragend="dragEnd(event)" draggable="true" id="'+ idPicture[0] + '-' + n +'" src="'+srcPicture+'" width="'+widthPicture+'">'); 
                $("#line"+state).addClass("styleLineTrue");
                dude.anims.play('right', true);
                one = 1;
            }
            else
            {
                $("#actContainer").prepend('<img ondragstart="dragStart(event)" ondragend="dragEnd(event)" draggable="true" id="'+ $("#state"+state+" img").attr("id") +'" src="'+$("#state"+state+" img").attr("src")+'" width="'+widthPicture+'">');
                $("#state"+state+" img").remove();    
            }
            break;
        case 2 :
            if(action == idPicture[0])
            {
                n++;
                $("#actContainer").prepend('<img ondragstart="dragStart(event)" ondragend="dragEnd(event)" draggable="true" id="'+ idPicture[0] + '-' + n +'" src="'+srcPicture+'" width="'+widthPicture+'">'); 
                $("#line"+state).addClass("styleLineTrue");
                two = 1;
            }
            else
            {
                $("#actContainer").prepend('<img ondragstart="dragStart(event)" ondragend="dragEnd(event)" draggable="true" id="'+ $("#state"+state+" img").attr("id") +'" src="'+$("#state"+state+" img").attr("src")+'" width="'+widthPicture+'">');
                $("#state"+state+" img").remove();    

            }
            break;
        case 3 : 
            if(action == idPicture[0])
            {
                n++;
                $("#actContainer").prepend('<img ondragstart="dragStart(event)" ondragend="dragEnd(event)" draggable="true" id="'+ idPicture[0] + '-' + n +'" src="'+srcPicture+'" width="'+widthPicture+'">'); 
                $("#line"+state).addClass("styleLineTrue");
                tree = 1;
            }
            else
            {
                $("#actContainer").prepend('<img ondragstart="dragStart(event)" ondragend="dragEnd(event)" draggable="true" id="'+ $("#state"+state+" img").attr("id") +'" src="'+$("#state"+state+" img").attr("src")+'" width="'+widthPicture+'">');
                $("#state"+state+" img").remove();  

            }
            break;
        case 4 : 
            if(action == idPicture[0])
            {
                n++;
                $("#actContainer").prepend('<img ondragstart="dragStart(event)" ondragend="dragEnd(event)" draggable="true" id="'+ idPicture[0] + '-' + n +'" src="'+srcPicture+'" width="'+widthPicture+'">'); 
                $("#line"+state).addClass("styleLineTrue");
                four = 1;
            }
            else
            {
                $("#actContainer").prepend('<img ondragstart="dragStart(event)" ondragend="dragEnd(event)" draggable="true" id="'+ $("#state"+state+" img").attr("id") +'" src="'+$("#state"+state+" img").attr("src")+'" width="'+widthPicture+'">');
                $("#state"+state+" img").remove();      

            }
            break;
        default : dude.setVelocityX(0);
    }
}

const config = {
    // type: Phaser.CANVAS,
    // parent: 'phaser-example',
    width: 1120,
    height: 600,
    display:'block',
    marginHorizontal: '0',
    backgroundColor: '#8FD0D6',
    type: Phaser.auto,
    parent: 'game',
    physics: {
        default: 'arcade',
        arcade: {
            gravity: {y:300},
            debug: true
        }
    },
    scene: {
        preload: preload,
        create: create,
        update: update,
    },
}

var game = new Phaser.Game(config);
let dude;

var left;
var right;
var up;
var down;
var platforms;



function preload() {
    this.load.spritesheet('dude', 'images/character.png', { frameWidth: 100, frameHeight: 74 });
    this.load.image('ground', 'images/ground.png');
}

function create() {


    platforms = this.physics.add.staticGroup();
    platforms.create(500, 600, 'ground').setScale(1, 1).refreshBody();

    dude = this.physics.add.sprite(48, 48, 'dude', 2);


    // Création d'une animation
    this.anims.create({
        key: 'up',
        frames: this.anims.generateFrameNumbers('dude', { start: 4, end: 4 }),
        frameRate: 10,
        repeat: 1
    });
    this.anims.create({
        key: 'down',
        frames: this.anims.generateFrameNumbers('dude', { start: 6, end: 6 }),
        frameRate: 10,
        repeat: 1
    });

    this.anims.create({
        key: 'left',
        frames: this.anims.generateFrameNumbers('dude', { start: 16, end: 18}),
        frameRate: 10,
        repeat: 1
    });
    this.anims.create({
        key: 'right',
        frames: this.anims.generateFrameNumbers('dude', { start: 13, end: 15 }),
        frameRate: 10,
        repeat: 5
    });



    // Ajout d'un rebont quand le personnage tombe
    dude.setBounce(0.2);

    // Empêche le personnage de sortir de l'écran 
    dude.setCollideWorldBounds(true);

    // Ajout la physique des platforms 
    this.physics.add.collider(dude, platforms);

    // Agrandissment de la carte 
    this.physics.world.setBounds(0, 0, 1500, 600);

    // La camera suit le personnage
    this.cameras.main.startFollow(dude);
    this.cameras.main.setBounds(0, 0, 1500, 600);



 
}



// function tween(target, x, y, duration){
//     var tween = this.scene.tweens.add({
//         targets: target,
//         x: x,
//         y: y,
//         duration: duration
//     });
// }

function update() {

    if(one === 1) {
        console.log("one");
        this.tweens.add({
            targets: dude,
            x: 500,
            duration: 2000
        });
        
        dude.anims.play('right', true);
        // tween(dude, 500,100,3000);
        one = 0;
    }
    else if(two === 1) {
        console.log('two');
        this.tweens.add({
            targets: dude,
            x: 800,
            y: 400,
            // z:0,
            duration: 3000
        });
        dude.anims.play('up', true);
        dude.body.setSize(25, 25, 25, 25);
        two = 0;
    }
    else if(tree === 1 ){
        console.log('tree');
        this.tweens.add({
            targets: dude,
            // x: 1100,
            y: 500,
            // z:0,
            duration: 10
        });

        this.tweens.add({
            targets: dude,
            x: 1100,
            // y: 500,
            // z:0,
            duration: 3000
        });
        dude.anims.play('right', true);
        dude.body.setSize(100, 75, 100, 100);
        tree = 0;
    }
    else if(four === 1 ){
        console.log('four');
        this.tweens.add({
            targets: dude,
            x: 800,
            y: 300,
            // z:1000,
            duration: 3000
        });
        dude.anims.play('left', true);
        four = 0;
    }
}
