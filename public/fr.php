<?php
	$lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
    if ($lang != 'fr') ;
    elseif 
    ($lang = 'br');
	header("Location: /br.php",TRUE,301);
 ?>

<!DOCTYPE html>
<meta name="language" content="fr" />

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="title" content="Algoschool">
    <meta name="description" content="Plateforme éducative qui a pour but d'aider les enfants à apprendre la logique derrière le code.">

    <link rel="shortcut icon" href="assets/img/favicon.png">

    <title>AlgoSchool | Plateforme éducative qui a pour but d'aider les enfants à apprendre la logique derrière le code.</title>

    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <link href="assets/css/bootstrap-theme.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/animations.css">

    <!-- siimple style -->
    <link href="assets/css/style.css" rel="stylesheet">
</head>



<body id="top" onselectstart="return false" oncontextmenu="return false" ondragstart="return false" onMouseOver="window.status='Il est strictement interdit de copier les images sur ce site'; return true;">
           
    <div class="cloud floating">
        <img src="assets/img/cloud.png" alt="AlgoSchool">
    </div>

    <div class="cloud pos1 fliped floating">
        <img src="assets/img/cloud.png" alt="AlgoSchool">
    </div>

    <div class="cloud pos2 floating">
        <img src="assets/img/cloud.png" alt="AlgoSchool">
    </div>

    <!-- <div class="cloud pos3 fliped floating">
        <img src="assets/img/cloud.png" alt="AlgoSchool">
    </div> -->


    <div id="wrapper">
    
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <img src="assets/img/logo.png" alt="AlgoSchool Logo" class="logo">
                    <br/>
                    <br/>
                    <?php 
 	                if ($_GET['lang']=='fr') {           // si la langue est 'fr' (français) on inclut le fichier fr-lang.php
                        echo "0";
                    } 
                    
                    else if ($_GET['lang']=='en') {      // si la langue est 'en' (anglais) on inclut le fichier en-lang.php
                        echo "1";
                    }
                    else {
                    ?>
                        <h2 class="subtitle">Futurs champions <?php echo $_GET['lang'] . "gege"; ?>, le site AlgoSchool sera bientôt disponible pour commencer l'aventure. 
                        <br>
                            Pendant qu'on est en travaux, vous pouvez nous découvrir sur les réseaux sociaux :</h2>
                        <br/>
                    <?php
                    }
                    ?>

                    
                </div>
                <div class="col-sm-12 align-center">
                    <ul class="social-network social-circle">
                        <li><a href="https://www.facebook.com/AlgoSchoolOfficial/" target="_blank" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a>
                        </li>

                        <li><a href="https://www.instagram.com/algoschool/" target="_blank" class="icoGit" title="Instagram"><i class="fa fa-instagram"></i></a>
                        </li>
                        <li><a href="https://www.linkedin.com/company/algoschool/" target="_blank" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            
        </div>
    </div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

    <script>
        $(document).ready( function () {
            $('#wrapper').height($(document).height());
            // I only have one form on the page but you can be more specific if need be.
            var $form = $('form');

            if ( $form.length > 0 ) {
                $('form input[type="submit"]').bind('click', function ( event ) {
                    if ( event ) event.preventDefault();
                    // validate_input() is a validation function I wrote, you'll have to substitute this with your own.
                    if ( $form.validate() ) { register($form); }
                });
            }
        });


        function appendResult(userText , className, iconClass){
          var resultHTML = "<div class='stretchLeft result "+ className + "'>" + userText + " <span class='fa fa-" + iconClass + "'></span>" + "</div>";
          $('body').append(resultHTML);
          $('.result').delay(10000).fadeOut('1000');
        }

    </script>

</body>

</html>
