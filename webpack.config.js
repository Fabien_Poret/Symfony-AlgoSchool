// webpack.config.js
var Encore = require('@symfony/webpack-encore');

Encore
    // the project directory where all compiled assets will be stored
    .setOutputPath('public/build/')

    // the public path used by the web server to access the previous directory
    // .setPublicPath('http://127.0.0.1:8000/build')
    .setPublicPath('/build')

    // will create public/build/app.js and public/build/app.css

    .addStyleEntry('schemaAlgo', './assets/scss/schemaAlgo.css')
    .addEntry('dnd', './assets/js/game/dnd.js')
    .addEntry('avancer', './assets/icoAct/icon-avancer.png')
    .addEntry('baisser', './assets/icoAct/icon-baisser.png')
    .addEntry('coup', './assets/icoAct/icon-coup.png')
    .addEntry('descendre', './assets/icoAct/icon-descendre.png')
    .addEntry('escalader', './assets/icoAct/icon-escalader.png')
    .addEntry('sauter', './assets/icoAct/icon-sauter.png')
    .addEntry('tenir', './assets/icoAct/icon-tenir.png')

    .addEntry('logo', './assets/images/logo.png')
    .addEntry('avatar', './assets/images/avatar.png')
    .addEntry('avatarAlgo', './assets/images/algoProf.png')
    .addEntry('bg', './assets/images/bg.png')
    .addEntry('test', './assets/video/video_bg.mp4')
    .addEntry('personnages', './assets/video/personnages.mp4')

    .addEntry('logo_algo', './assets/images/logo-algoschool.png')
    .addStyleEntry('createLvl', './assets/admin/scss/createLvl.scss')
    .addStyleEntry('userCreate', './assets/admin/scss/userCreate.scss')
    

    .addStyleEntry('normalize', './assets/admin/css/normalize.css')
    .addStyleEntry('bootstrap', './assets/admin/css/bootstrap.min.css')
    .addStyleEntry('awesome', './assets/admin/css/font-awesome.min.css')
    .addStyleEntry('themify', './assets/admin/css/themify-icons.css')
    // .addStyleEntry('flag', './assets/admin/css/flag-icon.min.css')
    .addStyleEntry('elastic', './assets/admin/css/cs-skin-elastic.css')
    .addStyleEntry('style', './assets/admin/scss/style.css')
    .addStyleEntry('jqvmap', './assets/admin/css/lib/vector-map/jqvmap.min.css')
    
    // AJOUT PJ *****************************************************
    // Plugin css 
    .addStyleEntry('popup', './assets/scss/plugin/magnific-popup.css')
    .addStyleEntry('animate', './assets/scss/plugin/animate.css')
    .addStyleEntry('responsive', './assets/scss/plugin/responsive.css')
   
    //   Images
    .addEntry('feature', './assets/images/theme/feature-image.png')
    .addEntry('feature-bg', './assets/images/theme/feature-bg.png')
    .addEntry('gal-1', './assets/images/theme/gallery-1.jpg')
    .addEntry('gal-2', './assets/images/theme/gallery-2.jpg')
    .addEntry('fond_bg', './assets/images/fond-bg.png')
    .addEntry('image_jeu', './assets/images/image_jeux.png')
    .addEntry('bg_section', './assets/images/bg_section2.png')
    .addEntry('hoverbtn', './assets/images/hoverbtn.png')
   .addEntry('transparent-reseau', './assets/images/image_reseau.gif')
   .addEntry('question', './assets/images/question.png')

   .addEntry('conversation', './assets/video/conversation-algo_1.gif')


    .addEntry('valide', './assets/images/valide.png')
    .addEntry('notValide', './assets/images/notValide.png')
   .addEntry('personnage', './assets/images/personnages.mp4')

   .addEntry('webdesigner2', './assets/images/webdesigner.png')
   .addEntry('marketeur', './assets/images/marketeur.png')
   .addEntry('graphiste', './assets/images/graphiste.png')
   .addEntry('webdesigner', './assets/images/developpeur.png')
   .addEntry('404', './assets/images/404.png')

   .addEntry('quentinhover', './assets/images/quentin-hover.png')
   .addEntry('nekahover', './assets/images/neka-hover.png')
   .addEntry('pjhover', './assets/images/pj-hover.png')
   .addEntry('fabienhover', './assets/images/fabien-hover.png')
   .addEntry('graphiques', './assets/images/graphique.gif')
   

   .addEntry('prof1', './assets/images/prof.png')
   .addEntry('prof2', './assets/images/prof2.png')
   .addEntry('algo1', './assets/images/algo.png')

   


    .addEntry('iphone', './assets/images/iphone.png')
    .addEntry('icon1', './assets/images/icon1.png')
    .addEntry('icon2', './assets/images/icon2.png')
    .addEntry('icon3', './assets/images/icon3.png')
    .addEntry('perso', './assets/images/perso.png')
    .addEntry('reseau', './assets/images/image_reseau.png')
    .addEntry('section5', './assets/images/section5.png')
    .addEntry('nuage', './assets/images/footer.png')
    .addEntry('android', './assets/images/android.png')
    .addEntry('ios', './assets/images/ios.png')
    .addEntry('fb', './assets/images/fb.png')
    .addEntry('in', './assets/images/in.png')
    .addEntry('deb', './assets/images/icon_debutant.png')
    .addEntry('prof', './assets/images/icon_prof.png')
    .addEntry('jeu', './assets/images/icon_jeu.png')
    .addEntry('ballon', './assets/images/montgolfiere.png')
    .addEntry('fonts', './assets/fonts/Riffic.ttf')
    .addEntry('fonts2', './assets/fonts/Riffic.svg')


    .addEntry('icon1gif', './assets/images/icon1.gif')
    .addEntry('icon2gif', './assets/images/icon2.gif')
    .addEntry('icon3gif', './assets/images/icon3.gif')

    .addEntry('quentin', './assets/images/quentin.png')
    .addEntry('fabien', './assets/images/fabien.png')
    .addEntry('pj', './assets/images/pj.png')
    .addEntry('neka', './assets/images/neka.png')

    //****************************************************************** */
    .addEntry('ajax', './assets/js/plugin/ajaxchimp.js')
    .addEntry('magnific', './assets/js/plugin/magnific-popup.min.js')
    .addEntry('main', './assets/js/plugin/main.js')
    .addEntry('carousseljs', './assets/js/plugin/owl.carousel.min.js')
    .addEntry('scrollUp', './assets/js/plugin/scrollUp.min.js')
    .addEntry('wow', './assets/js/plugin/wow.min.js')




    .addEntry('jquery', './assets/admin/js/vendor/jquery-2.1.4.min.js')
    .addEntry('plugins', './assets/admin/js/plugins.js')
    .addEntry('mainJS', './assets/admin/js/main.js')

    .addEntry('chart', './assets/admin/js/lib/chart-js/Chart.bundle.js')
    .addEntry('dashboard', './assets/admin/js/dashboard.js')
    .addEntry('widgets', './assets/admin/js/widgets.js')
    .addEntry('vmap', './assets/admin/js/lib/vector-map/jquery.vmap.js')
    .addEntry('vmapMin', './assets/admin/js/lib/vector-map/jquery.vmap.min.js')
    .addEntry('vmapData', './assets/admin/js/lib/vector-map/jquery.vmap.sampledata.js')
    .addEntry('vmapWorld', './assets/admin/js/lib/vector-map/country/jquery.vmap.world.js')
    .addEntry('createLvlJS', './assets/admin/js/createLvl.js')
    
    

    .addStyleEntry('header', './assets/scss/header.scss')
    .addStyleEntry('colors', './assets/scss/colors.scss')
    .addStyleEntry('contact', './assets/scss/contact.scss')
    .addStyleEntry('mainCss', './assets/scss/main.scss')
    .addStyleEntry('footer', './assets/scss/footer.scss')
    .addStyleEntry('myprodfil', './assets/admin/scss/myProfil.scss')

    //pages styles
    .addStyleEntry('showBlog', './assets/scss/pages/show.scss')
    .addStyleEntry('homepage', './assets/scss/pages/homepage.scss')
    .addStyleEntry('blog', './assets/scss/pages/blog.scss')

    // allow legacy applications to use $/jQuery as a global variable
    .autoProvidejQuery()

    // enable source maps during development
    .enableSourceMaps(!Encore.isProduction())

    // empty the outputPath dir before each build
    .cleanupOutputBeforeBuild()

    // show OS notifications when builds finish/fail
    .enableBuildNotifications()

    // create hashed filenames (e.g. app.abc123.css)
    .enableVersioning()

    // allow sass/scss files to be processed
    .enableSassLoader()
;

// export the final configuration
module.exports = Encore.getWebpackConfig();







    // --- Admin
    // .addStyleEntry('iCheckGreen', './assets/administration/iCheck/skins/flat/green.css')
    // .addStyleEntry('jqvmap', './assets/administration/jqvmap/dist/jqvmap.min.css')
    // .addStyleEntry('daterangepicker', './assets/administration/bootstrap-daterangepicker/daterangepicker.css')
    // .addStyleEntry('progressbar', './assets/administration/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css')
    
    // .addEntry('mainJS', './assets/js/main.js')
    // .addEntry('jqueryJS', './assets/administration/jquery/dist/jquery.min.js')
    // .addEntry('bootstrapJS', './assets/administration/bootstrap/dist/js/bootstrap.min.js')
    // .addEntry('fastclickJS', './assets/administration/fastclick/lib/fastclick.js')
    // .addEntry('nprogressJS', './assets/administration/nprogress/nprogress.js')
    // .addEntry('chartJS', './assets/administration/Chart.js/dist/Chart.min.js')
    // .addEntry('gaugeJS', './assets/administration/gauge.js/dist/gauge.min.js')
    // .addEntry('progressbarJS', './assets/administration/bootstrap-progressbar/bootstrap-progressbar.min.js')
    // .addEntry('iCheckJS', './assets/administration/iCheck/icheck.min.js')
    // .addEntry('skyconsJS', './assets/administration/skycons/skycons.js')
    // .addEntry('flotJS', './assets/administration/Flot/jquery.flot.js')
    // .addEntry('flotPieJS', './assets/administration/Flot/jquery.flot.pie.js')
    // .addEntry('flotTimeJS', './assets/administration/Flot/jquery.flot.time.js')
    // .addEntry('flotStackJS', './assets/administration/Flot/jquery.flot.stack.js')
    // .addEntry('flotResizeJS', './assets/administration/Flot/jquery.flot.resize.js')
    // .addEntry('flotOrderbarsJS', './assets/administration/flot.orderbars/js/jquery.flot.orderBars.js')
    // .addEntry('FlotSplineJS', './assets/administration/flot-spline/js/jquery.flot.spline.min.js')
    // .addEntry('curvedlinesJS', './assets/administration/flot.curvedlines/curvedLines.js')
    // .addEntry('dateJS', './assets/administration/DateJS/build/date.js')
    // .addEntry('jqvmapJS', './assets/administration/jqvmap/dist/jquery.vmap.js')
    // .addEntry('jqvmapWorldJS', './assets/administration/jqvmap/dist/maps/jquery.vmap.world.js')
    // .addEntry('jqvmapDataJS', './assets/administration/jqvmap/examples/js/jquery.vmap.sampledata.js')
    // .addEntry('momentJS', './assets/administration/moment/min/moment.min.js')
    // .addEntry('daterangepickerJS', './assets/administration/bootstrap-daterangepicker/daterangepicker.js')
    // .addEntry('customJS', './assets/administration/build/js/custom.min.js')
        
    // .addStyleEntry('fontAwesome', './assets/administration/font-awesome/css/font-awesome.min.css')
    // .addStyleEntry('nprogress', './assets/administration/nprogress/nprogress.css')
    // .addStyleEntry('animate', './assets/administration/animate.css/animate.min.css')
    // .addStyleEntry('bootstrap', './assets/administration/bootstrap/dist/css/bootstrap.css')
    // .addStyleEntry('custom', './assets/administration/build/css/custom.min.css')